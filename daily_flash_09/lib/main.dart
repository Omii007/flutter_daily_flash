import 'package:daily_flash_09/assignment2.dart';
import 'package:daily_flash_09/assignment3.dart';
import 'package:daily_flash_09/assignment4.dart';
import 'package:daily_flash_09/assignment5.dart';
import 'package:flutter/material.dart';

import 'assignment1.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment5(),
      debugShowCheckedModeBanner: false,
    );
  }
}
