

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State createState()=> _Assignment2State();
}

class _Assignment2State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Container(  
                  height: 100,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.network(
                            "https://imgs.search.brave.com/amXAlKHNvwuT0IdrXGrj2RBoU5BO9gHUEbNkoFVvcoc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMuY3RmYXNzZXRz/Lm5ldC9ocmx0eDEy/cGw4aHEvN29aWmx3/MGljYVY3UUxsaU1O/QjNPUS9mY2FlYTA1/ZGFiZGI4ZGU5YzBk/OWZmY2RjN2RmZDUw/NC90aHVtYl9hcHIy/Ml8wMS5qcGc",
                            height: 80,
                            width: 80,
                            
                          ),

                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.black),
                            ),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text("Nature",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),

                // 2
                Container(  
                  height: 100,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.network(
                            "https://imgs.search.brave.com/amXAlKHNvwuT0IdrXGrj2RBoU5BO9gHUEbNkoFVvcoc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMuY3RmYXNzZXRz/Lm5ldC9ocmx0eDEy/cGw4aHEvN29aWmx3/MGljYVY3UUxsaU1O/QjNPUS9mY2FlYTA1/ZGFiZGI4ZGU5YzBk/OWZmY2RjN2RmZDUw/NC90aHVtYl9hcHIy/Ml8wMS5qcGc",
                            height: 80,
                            width: 80,
                            
                          ),

                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.black),
                            ),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text("Nature",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // 3
                Container(  
                  height: 100,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.network(
                            "https://imgs.search.brave.com/amXAlKHNvwuT0IdrXGrj2RBoU5BO9gHUEbNkoFVvcoc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMuY3RmYXNzZXRz/Lm5ldC9ocmx0eDEy/cGw4aHEvN29aWmx3/MGljYVY3UUxsaU1O/QjNPUS9mY2FlYTA1/ZGFiZGI4ZGU5YzBk/OWZmY2RjN2RmZDUw/NC90aHVtYl9hcHIy/Ml8wMS5qcGc",
                            height: 80,
                            width: 80,
                            
                          ),

                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.black),
                            ),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text("Nature",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // 4
                Container(  
                  height: 100,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.network(
                            "https://imgs.search.brave.com/amXAlKHNvwuT0IdrXGrj2RBoU5BO9gHUEbNkoFVvcoc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMuY3RmYXNzZXRz/Lm5ldC9ocmx0eDEy/cGw4aHEvN29aWmx3/MGljYVY3UUxsaU1O/QjNPUS9mY2FlYTA1/ZGFiZGI4ZGU5YzBk/OWZmY2RjN2RmZDUw/NC90aHVtYl9hcHIy/Ml8wMS5qcGc",
                            height: 80,
                            width: 80,
                            
                          ),

                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.black),
                            ),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text("Nature",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // 5
                Container( 
                  height: 100, 
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.network(
                            "https://imgs.search.brave.com/amXAlKHNvwuT0IdrXGrj2RBoU5BO9gHUEbNkoFVvcoc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMuY3RmYXNzZXRz/Lm5ldC9ocmx0eDEy/cGw4aHEvN29aWmx3/MGljYVY3UUxsaU1O/QjNPUS9mY2FlYTA1/ZGFiZGI4ZGU5YzBk/OWZmY2RjN2RmZDUw/NC90aHVtYl9hcHIy/Ml8wMS5qcGc",
                            height: 80,
                            width: 80,
                            
                          ),

                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.black),
                            ),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text("Nature",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // 6
                Container(  
                  height: 100,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.network(
                            "https://imgs.search.brave.com/amXAlKHNvwuT0IdrXGrj2RBoU5BO9gHUEbNkoFVvcoc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMuY3RmYXNzZXRz/Lm5ldC9ocmx0eDEy/cGw4aHEvN29aWmx3/MGljYVY3UUxsaU1O/QjNPUS9mY2FlYTA1/ZGFiZGI4ZGU5YzBk/OWZmY2RjN2RmZDUw/NC90aHVtYl9hcHIy/Ml8wMS5qcGc",
                            height: 80,
                            width: 80,
                            
                          ),

                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.black),
                            ),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text("Nature",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // 6
                Container(  
                  height: 100,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.network(
                            "https://imgs.search.brave.com/amXAlKHNvwuT0IdrXGrj2RBoU5BO9gHUEbNkoFVvcoc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMuY3RmYXNzZXRz/Lm5ldC9ocmx0eDEy/cGw4aHEvN29aWmx3/MGljYVY3UUxsaU1O/QjNPUS9mY2FlYTA1/ZGFiZGI4ZGU5YzBk/OWZmY2RjN2RmZDUw/NC90aHVtYl9hcHIy/Ml8wMS5qcGc",
                            height: 80,
                            width: 80,
                            
                          ),

                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.black),
                            ),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text("Nature",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // 6
                Container(  
                  height: 100,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.network(
                            "https://imgs.search.brave.com/amXAlKHNvwuT0IdrXGrj2RBoU5BO9gHUEbNkoFVvcoc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMuY3RmYXNzZXRz/Lm5ldC9ocmx0eDEy/cGw4aHEvN29aWmx3/MGljYVY3UUxsaU1O/QjNPUS9mY2FlYTA1/ZGFiZGI4ZGU5YzBk/OWZmY2RjN2RmZDUw/NC90aHVtYl9hcHIy/Ml8wMS5qcGc",
                            height: 80,
                            width: 80,
                            
                          ),

                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.black),
                            ),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text("Nature",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}