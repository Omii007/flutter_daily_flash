


import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State createState()=> _Assignment3State();
}

class _Assignment3State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            
            // Container 1
            Container(
              padding: const EdgeInsets.all(10),
              decoration:  BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                border: Border.all(
                  color: Colors.black,
                  width: 5,
                ),
                boxShadow: const [
                  BoxShadow(
                    offset: Offset(10, 10),
                    blurRadius: 50,
                    spreadRadius: 20,
                    color: Colors.pink,
                  ),
                ],
              ),
              child: Column(
                children: [
                  Image.asset(
                    "assets/images/omkar.jpg"
                  ),
                  const Text("Omkar",
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}