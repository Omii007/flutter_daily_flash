

import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State createState()=> _Assignment2State();
}

class _Assignment2State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Container 1
            Container(
              height: 100,
              width: 100,
              child: Image.asset(
                "assets/images/image1.png",
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            // Container 2
            Container(
              height: 100,
              width: 100,
              child: Image.asset(
                "assets/images/image2.png",
              ),
            ),

            const SizedBox(
              height: 20,
            ),
            // Container 3
            Container(
              height: 100,
              width: 100,
              child: Image.asset(
                "assets/images/image3.png"
              ),
            ),
          ],
        ),
      ),
    );
  }
}