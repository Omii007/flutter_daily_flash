


import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State createState()=> _Assignment5State();
}

class _Assignment5State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Container 1
            Container(
              height: 100,
              width: 100,
              child: Image.asset(
                "assets/images/image1.png",
              ),
            ),
            const Spacer(),
            // Container 2
            Container(
              height: 100,
              width: 100,
              color: Colors.red,
            ),

            
            // Container 3
            Container(
              height: 100,
              width: 100,
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}