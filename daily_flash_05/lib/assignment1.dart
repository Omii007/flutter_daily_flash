
import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State createState()=> _Assignment1State();
}

class _Assignment1State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile Information",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/images/omkar.jpg",
              height: 250,
              width: 250,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("Omkar Ankush Machale",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16,              
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("Mobile No : 7350665314",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16,              
              ),
            ),
          ],
        ),
      ),
    );
  }
}