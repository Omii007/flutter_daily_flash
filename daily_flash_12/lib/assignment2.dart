

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State createState()=> _Assignment2State();
}

class _Assignment2State extends State <Assignment2>{

  List<String> weekList = [];
  final TextEditingController _weeknameController = TextEditingController();

  void onSubmit(String str){
    setState(() {
      weekList.add(str);
    });
  }

  String formatList(List<String> list) {
    return "[${list.join(", ")}]";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [

              const SizedBox(
                height: 50,
              ),
              TextFormField(
                onFieldSubmitted: (value) {
                  String str = _weeknameController.text;
                  if(value.trim().isNotEmpty){
                    onSubmit(str);
                  }else{
                    "Enter data";
                  }
                  _weeknameController.clear();
                  
                },
                controller: _weeknameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: const BorderSide(color: Colors.black),
                  ),
                  labelText: "Enter weekday"
                ),
              ),
              const SizedBox(
                height: 30,
              ),

              Expanded(
                child: ListView.builder(
                  
                  itemCount: weekList.length,
                  itemBuilder: (BuildContext context,int index){
                    return ListTile(
                    title: Text(weekList[index]),
                  );
                }),
              ),
            ],
          ),
        ),
      
    );
  }
}