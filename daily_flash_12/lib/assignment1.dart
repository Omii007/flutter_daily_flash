
import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  State createState()=> _Assignment1State();
}

class _Assignment1State extends State <Assignment1>{

  bool flag = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: TextFormField(
            obscureText: flag,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: const BorderSide(color: Colors.black),
              ),
              suffix: GestureDetector(
                onTap: () {
                  setState(() {
                    flag = !flag;
                  });
                },
                child: flag? const Icon(Icons.visibility_off) :const Icon(Icons.visibility) ,
              )
            ),
          ),
        ),
      ),
    );
  }
}