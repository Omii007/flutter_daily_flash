


import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State createState()=> _Assignment5State();
}

class _Assignment5State extends State <Assignment5>{

  final TextEditingController _dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [

              const SizedBox(
                height: 50,
              ),
              TextFormField(
                controller: _dateController,
                decoration: InputDecoration(
                  suffixIcon: const Icon(Icons.calendar_month_outlined),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: const BorderSide(color: Colors.black),
                  ),
                  labelText: "Enter date",
                  hintText: "Enter date"
                ),
                onTap: () async{
                  DateTime? dateTime = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(), 
                    firstDate: DateTime(2024), 
                    lastDate: DateTime(2025),
                  );
                  String dateFormat = DateFormat.yMMMd().format(dateTime!);

                  setState(() {
                    _dateController.text = dateFormat;
                  });
                },
                validator: (value) {
                  if(value!.trim().isEmpty){
                    return "please enter date";
                  }else{
                    return null;
                  }
                },
              ),
              const SizedBox(
                height: 30,
              ),

              
            ],
          ),
        ),
      
    );
  }
}