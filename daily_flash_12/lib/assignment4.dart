



import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State createState()=> _Assignment4State();
}
class NameClass{
  String name;
  String collegeName;

  NameClass({
    required this.name,
    required this.collegeName,
  });
}
class _Assignment4State extends State <Assignment4>{

  List<NameClass> nameList = [];
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _collegeController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  
  // void onSubmit(String str){
  //   setState(() {
  //     nameList.add(str);
  //   });
  // }

  String formatList(List<String> list) {
    return "[${list.join(", ")}]";
  }

  String text1 = "";
  String text2 = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: Padding(
          padding: const EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 50,
                ),
                TextFormField(
                  controller: _nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: const BorderSide(color: Colors.black),
                    ),
                    labelText: "Enter your name",
                    hintText: "Enter your name"
                  ),
                  validator: (value) {
                    if(value!.isEmpty || value.trim().isEmpty){
                      return "Enter your name";
                    }else{
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 30,
                ),

                // 2 
                TextFormField(
                  controller: _collegeController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: const BorderSide(color: Colors.black),
                    ),
                    labelText: "Enter college name",
                    hintText: "Enter college name"
                  ),
                  validator: (value) {
                    if(value!.isEmpty || value.trim().isEmpty){
                      return "Enter college name";
                    }else{
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 30,
                ),

                ElevatedButton(
                  onPressed: (){
                    setState(() {
                      bool validated = _formKey.currentState!.validate();
                      if(_nameController.text.trim().isNotEmpty && 
                        _collegeController.text.trim().isNotEmpty && validated){

                          nameList.add(
                            NameClass(
                            name: _nameController.text, 
                            collegeName: _collegeController.text
                          )
                          );
                        }
                        _nameController.clear();
                        _collegeController.clear();
                    });
                  }, 
                  child: const Text("Submit",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                ),

                Expanded(
                  child: ListView.builder(
                    itemCount: nameList.length,
                    itemBuilder: (BuildContext context, int index){
                      return Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Container(
                          height: 100,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                              width: 3,
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 5,
                              ),
                              Text("Name : ${nameList[index].name}",
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Text("College name : ${nameList[index].collegeName}",
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),  
    );
  }
}