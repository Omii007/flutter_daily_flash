


import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State createState()=> _Assignment3State();
}

class _Assignment3State extends State <Assignment3>{

  List<String> weekList = [];
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _collegeController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  void onSubmit(String str){
    setState(() {
      weekList.add(str);
    });
  }

  String formatList(List<String> list) {
    return "[${list.join(", ")}]";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: Padding(
          padding: const EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 50,
                ),
                TextFormField(
                  controller: _nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: const BorderSide(color: Colors.black),
                    ),
                    labelText: "Enter your name",
                    hintText: "Enter your name"
                  ),
                  validator: (value) {
                    if(value!.isEmpty || value.trim().isEmpty){
                      return "Enter your name";
                    }else{
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 30,
                ),

                // 2 
                TextFormField(
                  controller: _collegeController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: const BorderSide(color: Colors.black),
                    ),
                    labelText: "Enter college name",
                    hintText: "Enter college name"
                  ),
                  validator: (value) {
                    if(value!.isEmpty || value.trim().isEmpty){
                      return "Enter college name";
                    }else{
                      return null;
                    }
                  },
                ),

                const SizedBox(
                  height: 30,
                ),

                ElevatedButton(
                  onPressed: (){
                    setState(() {
                      bool validated = _formKey.currentState!.validate();

                    });
                  }, 
                  child: const Text("Submit",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      
    );
  }
}