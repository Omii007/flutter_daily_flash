

import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget{
  const Assignment5({super.key});

  @override
  State createState()=> _Assignment5State();
}

class _Assignment5State extends State {

  @override
  Widget build(BuildContext context){
    return 
    Scaffold(
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
            color: const Color.fromARGB(255, 219, 173, 173),
            borderRadius: BorderRadius.circular(20),
            boxShadow: const [
              BoxShadow(
                color: Colors.red,
                blurRadius: 10,
                spreadRadius: 3,
                offset: Offset(10, 10),
              ),
            ],
          ),
        ),
      ),
    );
  }
}