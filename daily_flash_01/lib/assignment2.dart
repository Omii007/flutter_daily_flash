import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget{
  const Assignment2({super.key});

  @override
  State createState()=> _Assignment2State();
}

class _Assignment2State extends State {

  @override
  Widget build(BuildContext context){
    return 
    Scaffold(
      appBar: AppBar(
        actionsIconTheme: const IconThemeData(color: Colors.black),
        title: const Text("Assignment1",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        leading: const Icon(Icons.menu,
          size: 35,
        ),
        centerTitle: true,
        actions: const [         
          Icon(Icons.search,
            size: 35,
          ),
          Icon(Icons.home,
            size: 35,
          ),
          Icon(Icons.person,
            size: 35,
          ),
        ],
        backgroundColor: Colors.blue,
      ),
    );
  }
}