


import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State createState()=> _Assignment4State();
}

class _Assignment4State extends State {

  bool flag = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            height: 200,
            width: 300,
            decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, -10),
                  color: Colors.black,
                  blurRadius: 3,
                  spreadRadius: 10,
                ),
              ],
            ),
            padding: const EdgeInsets.all(10),
          ),
        ),
      
    );
  }
}