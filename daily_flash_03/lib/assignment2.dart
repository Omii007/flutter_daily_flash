
import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State createState()=> _Assignment2State();
}

class _Assignment2State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 250,
          width: 450,
          decoration: BoxDecoration(
            image: const DecorationImage(
              image: AssetImage("assets/images/background1.jpg",
              ),
              fit: BoxFit.cover,
            ),
            border: Border.all(color: Colors.amber)
          ),
          
          child: const Center(
            child:  Text("Core2Web",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 20,
                color: Colors.white,
              ),
            ),
          )
        ),
      ),
    );
  }
}