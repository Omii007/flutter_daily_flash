



import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State createState()=> _Assignment5State();
}

class _Assignment5State extends State {

  bool flag = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            height: 300,
            width: 300,
            decoration: const BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
              gradient: LinearGradient(
                colors: [Colors.blue,Colors.red],
                stops: [0.5,0.5]
              ),
            ),
            padding: const EdgeInsets.all(10),
          ),
        ),
      
    );
  }
}