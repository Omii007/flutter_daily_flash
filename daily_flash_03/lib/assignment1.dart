
import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State createState()=> _Assignment1State();
}

class _Assignment1State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.amber)
          ),
          padding: const EdgeInsets.all(10),
          child: Image.asset(
            "assets/images/dark1.webp",
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}