

import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State createState()=> _Assignment3State();
}

class _Assignment3State extends State {

  bool flag = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              flag = !flag;
            });
          },
          child: Container(
            height: 300,
            width: 300,
            decoration: BoxDecoration(
              border: flag? Border.all(
                color: Colors.red,width: 5) : 
                Border.all(color: Colors.green,width: 5),
            ),
            padding: const EdgeInsets.all(10),
          ),
        ),
      ),
    );
  }
}