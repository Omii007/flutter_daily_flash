



import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State createState()=> _Assignment5State();
}

class _Assignment5State extends State<Assignment5>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
              colors: [Color.fromRGBO(55, 40, 215, 1),Color.fromRGBO(209, 105, 223, 0.624),Color.fromARGB(254, 29, 123, 32)],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,              
              //stops: [0.1,0.4]              
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.red,
                offset: Offset(12, 5),
              ),
            ],
          ),
        ),
      ),
    );
  }
}