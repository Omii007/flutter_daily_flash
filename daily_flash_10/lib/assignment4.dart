


import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State createState()=> _Assignment4State();
}

class _Assignment4State extends State<Assignment4>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            gradient: const LinearGradient(
              colors: [Colors.blue,Color.fromRGBO(199, 12, 143, 1)],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,              
              stops: [0.1,0.4]              
            ),
            boxShadow: const [
              BoxShadow(
                color: Colors.red,
                offset: Offset(10, 10),
              ),
            ],
          ),
        ),
      ),
    );
  }
}