
import 'dart:developer';

import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State createState()=> _Assignment5State();
}

class _Assignment5State extends State <Assignment5> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _mobileController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash 13",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 30,
                ),
                const Center(
                  child: Text("Login",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 35,
                      color: Colors.black,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
                const Text("User name",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 4,
                ),
                // Mobile No
                TextFormField(
                  controller: _mobileController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  
                  validator: (value) {
                    if(value!.trim().isEmpty){
                      return "Please entered username";
                    }else if(value.length < 6 || value.length > 20){
                      return "Username must be more than 6 letter";
                    }else{
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text("Password",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 4,
                ),
                // Email Id
                TextFormField(
                  // inputFormatters: [],
                  controller: _emailController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  validator: (value) {

                    if(value!.trim().isEmpty){
                      return "Please enter password";
                    }else if(value.length < 8){
                      return "Password must be 8 character";
                    }else if(!value.contains(RegExp(r'[A-Z]'))){
                      return "Password contains at least one capital letter";
                    }else if(!value.contains(RegExp(r'[a-z]'))){
                      return "Password contains at least one small letter";
                    }else if(!value.contains(RegExp(r'[0-9]'))){
                      return "Password contains at least one number letter";
                    }else if(!value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))){
                      return "Password contains at least one special letter";
                    }else{
                      return null;
                    }

                    // log("in validator");
                    // bool isNumber = false;
                    // bool isUpperchar = false;
                    // bool isLowerchar = false;
                    // bool isSpecial = false; 

                    // value!.codeUnits; // List of ASCII value

                    // if(value.trim().isEmpty){
                    //   return "Please entered password";
                    // }else if(value.length < 7){
                    //   return " Password must be 8 letter";
                    // }

                    // for(int i=0;i<value.codeUnits.length ;){
                    //   if(value.codeUnits[i] > 64 || value.codeUnits[i] < 91 ){
                    //     // isNumber = true;
                    //     // isSpecial = true;
                    //     // isLowerchar = true;
                    //     isUpperchar = true;
                    //   }else if(value.codeUnits[i] > 96 || value.codeUnits[i] < 123){ 
                    //     isLowerchar = true; 
                      
                    //   }else if(value.codeUnits[i] > 32 || value.codeUnits[i] < 65){
                    //     isNumber = true;
                    //     isSpecial = true;

                    //   }else{
                    //     return "Invalid Password";
                    //   }
                    //   // return null;
                    // }
                    // if(!isSpecial){
                    //   return "Must contain special character";
                    // }
                    // else if(!isNumber){
                    //   return "Must contain number";
                    // }
                    // else if(!isLowerchar){
                    //   return "Must contain lowercase";
                    // }
                    // else if(!isUpperchar){
                    //   return "Must contain uppercase";
                    // }else{
                    //   return null;
                    // } 
                    
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                Center(
                  child: ElevatedButton(
                    onPressed: (){
                      
                      setState(() {
                        bool validated = _formKey.currentState!.validate();
                              
                      });
                    }, 
                    style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.blue)
                    ),
                    child: const Text("Submit",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}