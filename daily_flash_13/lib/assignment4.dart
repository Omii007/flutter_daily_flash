



import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State createState()=> _Assignment4State();
}

class _Assignment4State extends State <Assignment4> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _mobileController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash 13",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 100,
                ),
                const Text("Mobile No",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 4,
                ),
                // Mobile No
                TextFormField(
                  controller: _mobileController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  maxLength: 10,
                  validator: (value) {
                    if(value!.trim().isEmpty){
                      return "Please entered mobile number";
                    }else if(value.length < 10){
                      return "Invalid mobile number";
                    }else{
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                const Text("Email Id",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 4,
                ),
                // Email Id
                TextFormField(
                  // inputFormatters: [],
                  controller: _emailController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  validator: (value) {
                    value!.codeUnits; // List of ASCII value
                    for(int i=0;i<value.length;i++){
                      if(value[i] == " "){
                        return "Invalid email id";
                      }
                    }
                    if(value.trim().isEmpty){
                      return "Please entered email id";
                    }else if(!value.endsWith("@gmail.com")){
                      return "Invalid email id";
                    }else{
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                Center(
                  child: ElevatedButton(
                    onPressed: (){
                      
                      setState(() {
                        bool validated = _formKey.currentState!.validate();
                              
                      });
                    }, 
                    style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.blue)
                    ),
                    child: const Text("Submit",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}