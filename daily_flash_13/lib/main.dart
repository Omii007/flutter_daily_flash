import 'package:daily_flash_13/assignment1.dart';
import 'package:daily_flash_13/assignment2.dart';
import 'package:daily_flash_13/assignment3.dart';
import 'package:daily_flash_13/assignment4.dart';
import 'package:daily_flash_13/assignment5.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment5(),
      debugShowCheckedModeBanner: false,
    );
  }
}
