


import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State createState()=> _Assignment3State();
}

class _Assignment3State extends State <Assignment3> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash 13",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              const SizedBox(
                height: 100,
              ),
              TextFormField(
                inputFormatters: [],
                controller: _textEditingController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
                validator: (value) {
                  value!.codeUnits; // List of ASCII value
                  for(int i=0;i<value.length;i++){
                    if(value[i] == " "){
                      return "Invalid email id";
                    }
                  }
                  if(value.trim().isEmpty){
                    return "Please entered data";
                  }else if(!value.endsWith("@gmail.com")){
                    return "Invalid email id";
                  }else{
                    return null;
                  }
                },
              ),
              const SizedBox(
                height: 50,
              ),
              ElevatedButton(
                onPressed: (){
                  
                  setState(() {
                    bool validated = _formKey.currentState!.validate();

                  });
                }, 
                style: const ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(Colors.blue)
                ),
                child: const Text("Submit",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}