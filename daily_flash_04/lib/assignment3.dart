


import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State createState()=> _Assignment3State();
}

class _Assignment3State extends State {

  int flag = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: (flag>= 1)? const Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
        
            Icon(Icons.person_outline_outlined),
            SizedBox(
              width: 5,
            ),
            Text("Omkar Ankush Machale",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 25,
                color: Colors.red,
              ),
            )
          ],
        ): const Row(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            flag++;
          });
        },
        child: const Icon(Icons.add,size: 35,),  
      ),
    );
  }
}