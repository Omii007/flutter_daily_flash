

import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State createState()=> _Assignment2State();
}

class _Assignment2State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.red)
          ),
          child: ElevatedButton(
            onPressed: (){},
            child: const Text(""),
          ),
        ),
      ),
    );
  }
}