
import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State createState()=> _Assignment1State();
}

class _Assignment1State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 50,
          width: 200,
          decoration: const BoxDecoration(
            boxShadow: [
              BoxShadow(
                offset: Offset(3, 3),
                color: Colors.red,
                spreadRadius: 3,
                blurRadius: 10,
              ),
            ]
          ),
          child: ElevatedButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.blue)
            ),
            onPressed: (){},
            child: const Text(""),
          ),
        ),
      ),
    );
  }
}