

import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State createState()=> _Assignment5State();
}

class _Assignment5State extends State {

  bool isPressed= false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Center(
            child: FloatingActionButton(
              backgroundColor: isPressed ? Colors.purple : Colors.white,
              onPressed: (){},
              child: GestureDetector(
                onLongPress:() {
                  setState(() {
                    isPressed = !isPressed;
                  });
                }, 
                onForcePressEnd: (details) {
                  setState(() {
                    isPressed;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}