import 'package:daily_flash_07/assignment2.dart';
import 'package:daily_flash_07/assignment3.dart';
import 'package:daily_flash_07/assignment5.dart';
import 'package:flutter/material.dart';

import 'assignment1.dart';
import 'assignment4.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment5(),
      debugShowCheckedModeBanner: false,
    );
  }
}
