



import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State createState() => _Assignment5State();
}

class _Assignment5State extends State {

  bool flag1 = true;
  bool flag2 = true;
  bool flag3 = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  flag1 = !flag1;
                });
              },
              child: Container(
                height: 100,
                width: 200,
                decoration: BoxDecoration(
                  color: flag1? Colors.white : Colors.red,
                  border: Border.all(
                    color: Colors.black,
                  ),
                ),
              ),
            ),

            GestureDetector(
              onTap: () {
                setState(() {
                  flag2 = !flag2;
                });
              },
              child: Container(
                height: 100,
                width: 200,
                decoration: BoxDecoration(
                  color: flag2? Colors.white : Colors.red,
                  border: Border.all(
                    color: Colors.black,
                  ),
                ),                
              ),
            ),

            GestureDetector(
              onTap: () {
                setState(() {
                  flag3 = !flag3;
                });
              },
              child: Container(
                height: 100,
                width: 200,
                decoration: BoxDecoration(
                  color: flag3? Colors.white : Colors.red,
                  border: Border.all(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}