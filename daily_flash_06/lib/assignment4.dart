


import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State createState() => _Assignment4State();
}

class _Assignment4State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 400,
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.black,
              
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: 150,
                width: 180,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                    
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                    height: 110,
                    width: 140,
                    color: Colors.red,
                  ),
                ),
              ),
              Container(
                height: 150,
                width: 180,                
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                  
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                    height: 110,
                    width: 140,
                    color: Colors.purple,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}