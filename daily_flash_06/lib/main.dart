import 'package:daily_flash_06/assignment1.dart';
import 'package:daily_flash_06/assignment2.dart';
import 'package:daily_flash_06/assignment3.dart';
import 'package:daily_flash_06/assignment4.dart';
import 'package:daily_flash_06/assignment5.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment5(),
      debugShowCheckedModeBanner: false,
    );
  }
}
