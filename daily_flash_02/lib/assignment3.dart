

import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State createState()=> _Assignment3State();
}

class _Assignment3State extends State {

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Center(
        child: Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            color: Colors.deepPurpleAccent,
            borderRadius: const BorderRadius.only(topRight: Radius.circular(20),),
            border: Border.all(
              color: Colors.red,
            ),
          ),
          
        ),
      ),
    );
  }
}