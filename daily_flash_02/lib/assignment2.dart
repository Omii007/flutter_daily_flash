

import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State createState()=> _Assignment2State();
}

class _Assignment2State extends State {

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: 200,
          width: 200,
          decoration: const BoxDecoration(
            color: Colors.blue,
            border: Border(
              left: BorderSide(
                width: 5,
                color: Colors.red,
              ),
            )
          ),
          child: const Center(
            child:  Text("OMKAR"),
          ),
        ),
      ),
    );
  }
}