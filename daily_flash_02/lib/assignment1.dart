
import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State createState()=> _Assignment1State();
}

class _Assignment1State extends State {

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: Colors.red,
            ),
          ),
          child: const Center(
            child:  Text("OMKAR"),
          ),
        ),
      ),
    );
  }
}