

import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State createState()=> _Assignment4State();
}

class _Assignment4State extends State {

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(20),
          height: 200,
          width: 300,
          decoration: BoxDecoration(
            color: const Color.fromARGB(255, 248, 157, 157),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
            border: Border.all(
              color: Colors.red,
            ),
          ),
          child: const Text("OMKAR"),
        ),
      ),
    );
  }
}