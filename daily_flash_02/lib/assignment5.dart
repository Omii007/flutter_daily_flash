

import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State createState()=> _Assignment5State();
}

class _Assignment5State extends State {

  bool flag = true;

  void flagChange(){
   
   flag = !flag;
  }

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Center(
        child :GestureDetector(
          onTap: (){
           setState(() {
              flagChange();
           });
          },
          child: Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
              color: (flag)? Colors.red : Colors.blue,
              borderRadius: BorderRadius.circular(20),
              border: Border.all( 
              ),
            ),
            child: Center(
              child: (flag)?const Text("Click me!",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ):  
              const Text("Container Tapped",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),               
              ),
            ),
          ),
        ),
      ),
    );
  }
}