
import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State createState()=> _Assignment1State();
}

class _Assignment1State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( 
        shape: const ContinuousRectangleBorder(side: BorderSide(color: Colors.black)),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 50,
              width: 50,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Color.fromARGB(255, 165, 217, 231)
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [         
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 200,
                  width: 150,
                  color: const Color.fromARGB(255, 240, 227, 86),
                ),
                Container(
                  height: 200,
                  width: 150,
                  color: const Color.fromARGB(255, 224, 93, 63),
                ),
              ],
            ),

            const SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                height: 150,
                width: double.infinity,
                color: Colors.greenAccent,
              ),
            ),

            const SizedBox(
              height: 35,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 200,
                  width: 150,
                  color: const Color.fromARGB(255, 111, 132, 200),
                ),
                Container(
                  height: 200,
                  width: 150,
                  color: const Color.fromARGB(255, 143, 108, 187),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}