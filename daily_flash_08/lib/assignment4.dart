


import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State createState()=> _Assignment4State();
}

class _Assignment4State extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: ListView(
        
          children: [
            // 1
            Container(
              padding: const EdgeInsets.all(5),
              height: 150,
              width: double.infinity,
               decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: const DecorationImage(image: NetworkImage(
                        "https://timesofindia.indiatimes.com/photo/104349866/104349866.jpg",
                        ),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(color: Colors.black),
                    ),
                    
                  ),
                  const Spacer(),
                  const Text("Rohit Sharma",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),

            // 2 
            Container(
              padding: const EdgeInsets.all(5),
              height: 150,
              width: double.infinity,
               decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: const DecorationImage(image: NetworkImage(
                        "https://i.pinimg.com/564x/d2/f5/0e/d2f50ec1ec206c280b056bb5873ad699.jpg",
                        ),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(color: Colors.black),
                    ),
                    
                  ),
                  const Spacer(),
                  const Text("Shubhman Gill",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ), 

            // 3
            Container(
              padding: const EdgeInsets.all(5),
              height: 150,
              width: double.infinity,
               decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: const DecorationImage(image: NetworkImage(
                        "https://i.pinimg.com/564x/7d/64/ac/7d64ac1fbce186a884e7ba3b154c0a68.jpg",
                        ),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(color: Colors.black),
                    ),
                    
                  ),
                  const Spacer(),
                  const Text("Virat Kohli",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),

            // 4
            Container(
              padding: const EdgeInsets.all(5),
              height: 150,
              width: double.infinity,
               decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: const DecorationImage(image: NetworkImage(
                        "https://i.pinimg.com/736x/8d/93/a3/8d93a3ec92d874f9dccd432379610965.jpg",
                        ),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(color: Colors.black),
                    ),
                    
                  ),
                  const Spacer(),
                  const Text("KL Rahul",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),

            // 5
            Container(
              padding: const EdgeInsets.all(5),
              height: 150,
              width: double.infinity,
               decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: const DecorationImage(image: NetworkImage(
                        "https://images.deccanherald.com/deccanherald/2023-08/7763a241-bda2-4752-8f5d-c3cad7d9cc69/PTI01_12_2023_000406A.jpg?w=1200&h=675&auto=format%2Ccompress&fit=max&enlarge=true",
                        ),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(color: Colors.black),
                    ),
                    
                  ),
                  const Spacer(),
                  const Text("KL RAHUL",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),

            //6
            Container(
              padding: const EdgeInsets.all(5),
              height: 150,
              width: double.infinity,
               decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: const DecorationImage(image: NetworkImage(
                        "https://toppng.com/uploads/preview/hardik-pandya-india-bcci-cricket-odi-icc-champions-hardik-pandya-11563176115osoe1y1o8g.png",
                        ),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(color: Colors.black),
                    ),
                    
                  ),
                  const Spacer(),
                  const Text("Hardik Pandya",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),

            // 7
            Container(
              padding: const EdgeInsets.all(5),
              height: 150,
              width: double.infinity,
               decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: const DecorationImage(image: NetworkImage(
                        "https://wallpapers.com/images/hd/proud-ravindra-jadeja-892vjvwhacdhk4hi.jpg",
                        ),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(color: Colors.black),
                    ),
                    
                  ),
                  const Spacer(),
                  const Text("Ravendra Jadega",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
            // 8
            Container(
              padding: const EdgeInsets.all(5),
              height: 150,
              width: double.infinity,
               decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: const DecorationImage(image: NetworkImage(
                        "https://i.pinimg.com/originals/51/01/e5/5101e5fbdcbd5ab7817c2d8b0bd82996.jpg",
                        ),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(color: Colors.black),
                    ),
                    
                  ),
                  const Spacer(),
                  const Text("Mohamad Siraj",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}